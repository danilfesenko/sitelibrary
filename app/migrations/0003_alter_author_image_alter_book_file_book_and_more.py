# Generated by Django 5.0 on 2024-03-03 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_alter_author_options_remove_book_firs_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='image',
            field=models.ImageField(upload_to='', verbose_name='Изоброжение'),
        ),
        migrations.AlterField(
            model_name='book',
            name='file_book',
            field=models.FileField(upload_to='book/', verbose_name='Книга в PDF'),
        ),
        migrations.AlterField(
            model_name='book',
            name='image',
            field=models.ImageField(upload_to='', verbose_name='Обложка'),
        ),
    ]
