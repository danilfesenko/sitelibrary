# Generated by Django 5.0.2 on 2024-03-08 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_genres_book_genres'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='rating',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=7),
        ),
    ]
