from django.shortcuts import get_list_or_404, get_object_or_404, redirect, render
from django.http import FileResponse, HttpResponseRedirect
from django.template.defaulttags import register

from django.db.models import Count
from django.core.paginator import Paginator
from django.db.models import Avg
from app.models import Author, Book, Genres
from comments.models import Comments


def book_pdf(request, book_id):
    
    book = get_object_or_404(Book, id=book_id)
    pdf_file = book.file_book.path
    return FileResponse(open(pdf_file, 'rb'), content_type='application/pdf')

    
def indexpage(request):
    romany = Book.objects.filter(genres__slug='romany')[:4]
    bestsellery =  Book.objects.filter(genres__slug='bestsellery')[:4]
    context = {
        "romany": romany,
        "bestsellery": bestsellery
    }
    
    return render(request, "index.html", context)

def authorpage(request):
    authors = Author.objects.order_by('firs_name')
    
    context = {
        "authors": authors,
    }
    
    return render(request, "authors.html", context=context)

def update_book_rating(book):
    reviews = Comments.objects.filter(book=book)
    if reviews.count() > 0:
        rating_avg = reviews.aggregate(Avg('rating'))['rating__avg']
        book.rating = round(rating_avg, 2)
    else:
        book.rating = 0.00
    book.save()
    
def bookpage(request, book_id):
    
    book = Book.objects.get(id=book_id)
    comments = Comments.objects.filter(book_id=book_id).order_by('-data')
    
    if request.method == 'POST':
        rating = request.POST.get('rating')
        comment = request.POST.get('comment')
        Comments.objects.create(book=book, user=request.user, rating=rating, text=comment)
        update_book_rating(book)
    
    context = {
        'books': book,
        'comments': comments,
        'rating': int(book.rating)
    }
    
    return render(request, "page_book.html", context=context)

def bookspage(request, genre_slug='all'):
    
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', None)
    
    if genre_slug == 'all':
        books = Book.objects.all()
    else:
        books = get_list_or_404(Book.objects.filter(genres__slug=genre_slug))
    
    # Фильтры    
    if order_by and order_by != 'popular':
        books = books.order_by(order_by)
        
    elif order_by == 'popular':
        books = books.annotate(num_favorites=Count('favoritebook'))
        books = books.order_by('-num_favorites')
        
    paginator = Paginator(books, 8)
    current_page = paginator.page(page)
        
    genres = Genres.objects.all()
    context = {
        'books': current_page,
        'genres': genres,
        'slug_url': genre_slug
    }
    
    return render(request, "books.html", context=context)

def authoprofrpage(request, author_id):
    
    author = Author.objects.get(id=author_id)
    books = Book.objects.filter(id_author_id=author_id)
    
    context = {
        'author': author,
        'books': books,
    } 

    return render(request, "page_author.html", context=context) 

@register.filter
def get_range(value):
    return range(value)