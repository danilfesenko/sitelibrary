from django.db import models

from login.models import User

# Create your models here.
class Author(models.Model):
    firs_name = models.CharField(max_length=30, verbose_name='Имя')
    last_name = models.CharField(max_length=30, verbose_name='Фамилия')
    bio = models.CharField(max_length=200, verbose_name='Биогрфия')
    image = models.ImageField(upload_to='', verbose_name='Изоброжение')
    
    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"
    
    def __str__(self):
        return f'{self.firs_name} {self.last_name}'

class Genres(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name="Название")
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True, verbose_name="URL")
    
    class Meta:
        db_table = 'genres'
        verbose_name = "Жанра"
        verbose_name_plural = "Жанры"
        
    def __str__(self):
        return self.name

class Book(models.Model):
    id_author = models.ForeignKey(to=Author, on_delete=models.CASCADE, verbose_name='Автор')
    genres = models.ManyToManyField(Genres, verbose_name="Жанры")
    title = models.CharField(max_length=100, verbose_name='Название')
    description = models.CharField(max_length=500, verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Обложка')
    file_book = models.FileField(upload_to='book/', verbose_name='Книга в PDF')
    rating = models.DecimalField(default=0.00, max_digits=7, decimal_places=2, verbose_name="Рейтинг")
    
    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"
        ordering = ("id",)
        
    def __str__(self):
        return f'{self.title} - {self.id_author.firs_name} {self.id_author.last_name}'
    

class FavoriteBook(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name = "Избранное"
        verbose_name_plural = "Избранные"
        
    def __str__(self):
        return f'{self.user.username} - {self.book.title}'