from django.contrib import admin

from app.models import Author, Book, FavoriteBook, Genres

# Register your models here.
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(FavoriteBook)

@admin.register(Genres)
class GenresAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}