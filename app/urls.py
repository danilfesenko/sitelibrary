﻿from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.views.generic import TemplateView

from app import views

app_name = 'app'

urlpatterns = [
    path('', views.indexpage, name='index'),
    path('author/', views.authorpage, name='author'),
    path('books/', views.bookspage, name='books'),
    path('books/<slug:genre_slug>', views.bookspage, name='books_slug'),
    path('book/<int:book_id>/', views.bookpage, name='book'),
    path('book/<int:book_id>/pdf/', views.book_pdf, name='book_pdf'),
    path('author/<int:author_id>/', views.authoprofrpage, name='author_page'),
]


