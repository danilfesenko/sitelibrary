import os
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib import auth, messages
from django.urls import reverse
from django.db.models import Prefetch
from app.models import Book, FavoriteBook
from django.views.decorators.http import require_POST
 
from login.forms import ProfileForm, UserLoginForm, UserRegForm
from django.contrib.auth.decorators import login_required


from login.forms import UserLoginForm
from login.models import User

# Create your views here.
def loginpage(request):
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(username=username, password=password)
            
            if user:
                auth.login(request,user)
                messages.success(request, f'{username}, Вы вошли в аккаунт')
                return HttpResponseRedirect(reverse('main:index'))
    else:
        form = UserLoginForm()
    context = {
        'form' : form
    }
    return render(request, 'login.html', context)

def regpage(request):
    if request.method == 'POST':
        form = UserRegForm(data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('login:login'))
    else:
        form = UserRegForm()
        
    context = {
        'form': form
    }
    return render(request, 'reg.html', context)

@login_required
def profilepage(request):
    
    if request.method == 'POST':
        form = ProfileForm(data=request.POST, instance=request.user, files=request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('login:profile'))
    else:
        form = ProfileForm(instance=request.user)
    
    favorite_books = FavoriteBook.objects.filter(user=request.user)
    
    context = {
        'form': form,
        'favorite_books': favorite_books
    }
    return render(request, 'profile.html', context)

@require_POST
def add_to_favorite(request):
    book_id = request.POST.get('book_id')
    book = Book.objects.get(id=book_id)
    user = request.user
    
    if not FavoriteBook.objects.filter(user=user, book=book).exists():
        FavoriteBook.objects.create(user=user, book=book)
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@require_POST
def del_favorite(request):
    book_id = request.POST.get('book_id')
    book = Book.objects.get(id=book_id)
    user = request.user
    
    favorite_book = FavoriteBook.objects.filter(user=user, book=book)
    
    if favorite_book.exists():
        favorite_book.delete()
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def logout(request):
    messages.success(request, 'Вы вышли из аккаунта')
    auth.logout(request)
    
    return redirect(reverse('main:index'))