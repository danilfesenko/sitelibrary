﻿from django.urls import path

from login import views

app_name = 'login'

urlpatterns = [
    path('login/', views.loginpage, name='login'),
    path('reg/', views.regpage, name='reg'),
    path('profile/', views.profilepage, name='profile'),
    path('logout/', views.logout, name='logout'),
    path('add_to_favorite/', views.add_to_favorite, name='add_to_favorite'),
    path('del_favorite/', views.del_favorite, name='del_favorite'),
]


