from django.db import models
from app.models import Book

from login.models import User

# Create your models here.
class Comments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор')
    book = models.ForeignKey(Book, on_delete=models.CASCADE, verbose_name='Книга')
    text = models.CharField(max_length=300, verbose_name='Коментарий')
    rating = models.IntegerField(choices=[(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')])
    data = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = 'comments'
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        
    def __str__(self):
        return f'{self.user.username} | {self.book.title} | {self.data}'