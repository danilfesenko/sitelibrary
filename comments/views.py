from django.http import HttpResponseRedirect
from django.shortcuts import render
from app.models import Book
from app.views import update_book_rating
from comments.forms import CommentsADD

from comments.models import Comments

def del_com(request):
    if request.method == "POST":
        comment_id = request.POST.get('comment_id')
        comment = Comments.objects.get(id=comment_id)
    
        comment.delete()
        update_book_rating(comment.book)
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))