﻿from django.urls import path

from comments import views

app_name = 'comments'

urlpatterns = [
    path('del_com/', views.del_com, name='del_com'),
]
