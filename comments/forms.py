﻿from django import forms

from comments.models import Comments

class CommentsADD(forms.Form):
    class Meta:
        model = Comments
        fields = (
            "text",
            "rating",
        )
    text = forms.CharField()
    rating = forms.CharField()