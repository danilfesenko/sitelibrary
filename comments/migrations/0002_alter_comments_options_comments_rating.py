# Generated by Django 5.0.2 on 2024-03-10 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comments', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comments',
            options={'verbose_name': 'Отзыв', 'verbose_name_plural': 'Отзывы'},
        ),
        migrations.AddField(
            model_name='comments',
            name='rating',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
